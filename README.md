# solenoid_driver



## Basics

A driver board to control a solenoid with a spike and hold.

Initially we are developing a driver based on the Low-Fet driver from [modular things](https://github.com/modular-things/modular-things).