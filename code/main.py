from machine import Pin, PWM, Timer
from time import sleep, sleep_us

# constants
spike_duration = 3.5 # spike duration, ms
pulse_duration = 500 # dwell (including spike time), ms
total_cycle = 3000  # period of the cycle, ms

sd_math = int(spike_duration*1000)
pd_math = int((pulse_duration-spike_duration)*1000)

# establish pins
led_pin = Pin(27, Pin.OUT)
sol_pin = Pin(29, Pin.OUT)

# establish timer object
timer = Timer() # timer object

# initiate variables
duty = 0 # duty cycle, %
duty_percent = 15
hold_duty = round(duty_percent/100*65535) # reduced duty cycle, %

# pwm object, off
pwm_led = PWM(led_pin, freq=10000, duty_u16=duty)
pwm_sol = PWM(sol_pin, freq=100000, duty_u16=duty)

def pwm_control(timer):

    # on cycle
    pwm_led.duty_u16(65535) # set the duty cycle of the led
    pwm_sol.duty_u16(65535) # set the duty cycle of the solenoid
    sleep_us(sd_math)
    
    # pwm cycle
    pwm_led.duty_u16(hold_duty) # set the duty cycle of the led
    pwm_sol.duty_u16(hold_duty) # set the duty cycle of the solenoid
    sleep_us(pd_math)
    
    # off cycle
    pwm_led.duty_u16(0) # set the duty cycle of the led
    pwm_sol.duty_u16(0) # set the duty cycle of the solenoid
    
print("initiate runtime")

try:
    # timer initialize
    timer.init(period=total_cycle, mode=Timer.PERIODIC, callback=pwm_control)

    # allocate for extra application functionality
    while True:
        pass
        sleep(10)

except KeyboardInterrupt:
    # catch a soft reboot or keyboard interrupt
    print("omg")
    

# turn off the dang pins! this for your own good
# (unless you want a burnt out solenoid)
finally:
    print("purge")
    
    # turn off the main timer
    timer.deinit()

    # set pwm duty cycle to 0, this way needs a delay before
    # the pwm.deinit() call unfortunately
    #pwm_led.duty_u16(0) # set the duty cycle of the led
    #pwm_sol.duty_u16(0) # set the duty cycle of the solenoid
    #time.sleep(0.1)
    
    # turn off the pwm timers
    pwm_led.deinit()
    pwm_sol.deinit()
    
    
    # reinitialize pins as digital outputs
    led_pin.init(Pin.OUT)
    sol_pin.init(Pin.OUT)
    
    # set digital pins to 0
    led_pin.value(0)
    sol_pin.value(0)

    
    print("purge complete")
    
    
